import os
import pandas as pd
import numpy as np
from dotenv import load_dotenv
import mysql.connector
from mysql.connector import Error
from influxdb import InfluxDBClient

def influxdb(host, database, table, device, channel, port=8086):
    """
        Get the high frequency vibration data by device and channel.

        Parameters
        ----------
        host : str, ip
        port : int, default 8086
        table : str, table name in database 
        device : str, login https://pms.fpcetg.com.tw/locations can get device ID, like Machow_group01_dev001
        channel : str, ch01, ch02, ...

        Returns
        -------
        df_result : pandas.DataFrame

        Example
        -------
        result = influxdb("XX.XX.XX.XX", "abc", "record_raw", "Machow_group01_dev001", "ch01", "2022-04-13 08:00:00")
    """
    client = InfluxDBClient(host=host, port=port, database=database) 
    client.get_list_database()
    result = client.query("select * from {0} where time > '{1}' order by time".format(table))      
    result_list = list(result.get_points())
    df_result = pd.DataFrame(result_list)
    df_result = df_result[(df_result["deviceCode"]==device) & (df_result["channelCode"]==channel)]
    return df_result

def mariadb(host, database, user, password, table):
    """
        Get the monitoring platform information.

        Parameters
        ----------
        host : str, ip
        database : str, database name
        user : str, database account ID
        password : str, database account password
        table : str, table name in database 

        Returns
        -------
        df_result : pandas.DataFrame

        Example
        -------
        result = mariadb('XX.XX.XX.XX', 'abc', 'cde', 'efg', 'devices')
    """
    try:
        # 連接 MySQL/MariaDB 資料庫
        connection = mysql.connector.connect(
            host=host,          # 主機名稱
            database=database,  # 資料庫名稱
            user=user,          # 帳號
            password=password)  # 密碼

        if connection.is_connected():

            # # 顯示資料庫版本
            # db_Info = connection.get_server_info()
            # print("資料庫版本：", db_Info)

            # # 顯示目前使用的資料庫
            # cursor = connection.cursor()
            # cursor.execute("SELECT DATABASE();")
            # record = cursor.fetchone()
            # print("目前使用的資料庫：", record)
            
            # 查詢資料庫
            cursor = connection.cursor()
    #         cursor.execute("SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES ORDER BY TABLE_NAME")
            cursor.execute("SELECT * FROM {0}".format(table))
            records = cursor.fetchall()

            # 列出查詢的資料
            data = pd.DataFrame(records, columns=[x[0] for x in cursor.description])
            # data.to_csv("data.csv", index=False)
            
            return data

    except Error as e:
        print("資料庫連接失敗：", e)

    finally:
        if (connection.is_connected()):
            cursor.close()
            connection.close()
            print("資料庫連線已關閉")